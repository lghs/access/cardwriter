# LGHS Card Writer 

## upload spiffs to esp
add password to data before uploading
```bash
pio run -t uploadfs
```

## Troubleshoot
### `SPIFFS.begin()` issue
When not in main.cpp, `SPIFFS.begin()` crash.
