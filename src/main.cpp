#include <Arduino.h>
#include <SPIFFS.h>
#include <Wifi.h>
#include "RFIDCardReader.h"
#include <FastLED.h>
#include <VirtualLed.h>
#include "Constants.h"
#include "WriteCardReadHook.h"
#include "WifiHookFeedback.h"
#include "CardWriterConfig.h"
#include <iostream>

CRGB LED[NUM_LEDS];
VirtualLed wifiIndicator(0, 1);
VirtualLed cardReadIndicator(1, 8);

CardWriterConfig config;
Wifi wifi;
WifiHookFeedback wifiHookFeedback(&wifiIndicator);

WriteCardReadHook cardWriteHook(&cardReadIndicator);
RFIDCardReader accessCardReader(RFID_SS_PIN, RFID_RST_PIN, &cardWriteHook);

TaskHandle_t retrieveAndWriteLoopHandle;

void retrieveAndWriteLoop(void *noParam) {
    for (;;) {
        cardWriteHook.run();
        delay(100);
    }
}
void setup() {
    Serial.begin(115200);
    Serial.println("Booted");
    //has to be in main.cpp or fail
    if (!SPIFFS.begin()) {
        throw std::runtime_error("An Error has occurred while mounting SPIFFS");
    }

    FastLED.setBrightness(NUM_LEDS);
    FastLED.addLeds<WS2812B, LED_PIN, GRB>(LED, NUM_LEDS);
    config.setup();

    wifi.setup(config.ssid(), config.wifiPassword(), config.hostname());
    wifi.setWifiStatusHook(&wifiHookFeedback);

    accessCardReader.setup();
    cardWriteHook.setup(string(config.basePayloadServiceUrl()), &accessCardReader);
    cardWriteHook.reset();

    xTaskCreatePinnedToCore(&retrieveAndWriteLoop, "retrieveAndWriteLoop",10000,NULL,5000,&retrieveAndWriteLoopHandle,0);


}
void loop() {
    wifi.run();
    accessCardReader.run();
    wifiIndicator.applyColor(LED);
    cardReadIndicator.applyColor(LED);
    FastLED.show();
}