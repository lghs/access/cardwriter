

#include <esp_system.h>
#include <Arduino.h>
#include "Secret.h"

const string Secret::ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

string Secret::generateSecret() {
    char secret[SECRET_SIZE+1];
    for(int i = 0; i<SECRET_SIZE; i++){
        secret[i] = Secret::ALPHABET[esp_random() % ALPHABET.length()];
    }
    secret[SECRET_SIZE] = '\0';
    return string(secret);
}