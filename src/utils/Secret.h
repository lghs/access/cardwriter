

#pragma once
#include <string>

using namespace std;

#define SECRET_SIZE 30
class Secret {
public:
    const static string ALPHABET;
    static string generateSecret();
};



