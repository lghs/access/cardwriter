#pragma once
//RFID
#define RFID_RST_PIN 0
#define RFID_SS_PIN  5

//LED
#define FASTLED_INTERNAL
#define NUM_LEDS 9
#define FASTLED_ALLOW_INTERRUPTS 0
#define LED_PIN 14
