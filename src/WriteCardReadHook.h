#pragma once

#include <pattern/BlinkLedColorPattern.h>
#include <VirtualLed.h>
#include <CardReadHook.h>
#include <AccessCard.h>
#include <CardParser.h>
#include <RFIDCardReader.h>
#include "LedUtils.h"

#define RETRY_RETRIEVE_PAYLOAD 5000

class WriteCardReadHook : public CardReadHook {

    BlinkLedColorPattern successWriteCardPattern = BlinkLedColorPattern(GREEN, SUPERFAST);
    BlinkLedColorPattern errorPattern = BlinkLedColorPattern(RED, SUPERFAST);
    BlinkLedColorPattern retrievingPayloadPattern = BlinkLedColorPattern(BLUE, SLOW);
    BlinkLedColorPattern writingProcessPattern = BlinkLedColorPattern(BLUE, SUPERFAST);


    long cardToRetrieve = 0;
    RFIDCardPayload payloadToWrite;
    string secret;

    string basePayloadServiceUrl = "";

    VirtualLed *indicatorLed;
    RFIDCardReader *cardReader;

    void writeCard(RFIDCardPayload payload);

    void error(string m);

    long lastRetrieveTry = 0;

public:

    WriteCardReadHook(VirtualLed *indicatorLed);

    void onRead(RFIDCardPayload payload) override;

    void setup(string basePayloadServiceUrl, RFIDCardReader *cardReader);

    void run();

    void reset();

};


