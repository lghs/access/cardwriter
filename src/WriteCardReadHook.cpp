

#include <utils/Secret.h>
#include <HTTPClient.h>
#include "WriteCardReadHook.h"

WriteCardReadHook::WriteCardReadHook(VirtualLed *indicatorLed)
        : indicatorLed(indicatorLed) {
}

void WriteCardReadHook::onRead(RFIDCardPayload payload) {

    Serial.println(("Card read:" + to_string(payload.cardId)).c_str());
    if (payloadToWrite.cardId) {
        writeCard(payloadToWrite);
    } else {
        cardToRetrieve = payload.cardId;
        if(secret == string("")){
            secret= Secret::generateSecret();
            string url = basePayloadServiceUrl + "/a?s=" + secret;
            Serial.println(url.c_str());
        }
        indicatorLed->setPattern(&retrievingPayloadPattern);
    }
}


void WriteCardReadHook::writeCard(RFIDCardPayload payload) {
    try {
        if (payloadToWrite.cardId != payload.cardId) {
            error("wrong card");
        } else if (payloadToWrite.cardId == payload.cardId) {
            cardReader->writeCard(payload.cardContent);
            Serial.println("Card written");
            indicatorLed->setPattern(&successWriteCardPattern, 1000);
            reset();

        }
    } catch (std::runtime_error e) {
        error(("Error while reading card:" + string(e.what())));

    } catch (...) {
        error("Error while reading card");
    }
}

void WriteCardReadHook::reset() {
    payloadToWrite = RFIDCardPayload();
    cardToRetrieve = 0;
    secret = string("");
    if (indicatorLed->getPattern() != &successWriteCardPattern) {
        indicatorLed->setPattern(NULL);
    }
}


void WriteCardReadHook::error(string m) {
    Serial.println(m.c_str());
    indicatorLed->setPattern(&errorPattern, 1000, true);
}

void WriteCardReadHook::setup(string basePayloadServiceUrl, RFIDCardReader *cardReader) {
    this->basePayloadServiceUrl = basePayloadServiceUrl;
    this->cardReader = cardReader;
}

void WriteCardReadHook::run() {
    if (cardToRetrieve && (millis() - RETRY_RETRIEVE_PAYLOAD) > lastRetrieveTry ) {
        HTTPClient http;
        string url = basePayloadServiceUrl + "/api/payload/?secret=" + secret + "&cardId=" + to_string(cardToRetrieve);
        http.begin(url.c_str());
        lastRetrieveTry = millis();
        if (http.GET() == 200) {
            payloadToWrite = RFIDCardPayload(cardToRetrieve, string(http.getString().c_str()));
            cardToRetrieve = 0;
            indicatorLed->setPattern(&writingProcessPattern);
        } else {
            Serial.println("Error while recovering payload " + http.getString());
        }
    }
}